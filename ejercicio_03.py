"""
Escribe un programa en Python que solicite al usuario que 
ingrese 5 números enteros. Luego imprimir el máximoy el 
mínimo de los valores ingresados. El programa deberá
permitir el ingreso de valores iguales.
"""

numeros_ingresados = []

for i in range(5):
    numero_ingresado = int(input('Ingrese un número entero: '))
    numeros_ingresados.append(numero_ingresado)

numero_maximo = max(numeros_ingresados)
numero_minimo = min(numeros_ingresados)
print(f'El máximo valor ingresado es: {numero_maximo}')
print(f'El mínimo valor ingresado es: {numero_minimo}')
