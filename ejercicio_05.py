"""
Escribir una función que permita calcular la duración
en segundos de un intervalo dado en horas, minutos y 
segundos.
"""

def calcular_duracion_en_segundos(horas, minutos, segundos):
    segundos_totales = horas * 3600 + minutos * 60 + segundos
    return segundos_totales
