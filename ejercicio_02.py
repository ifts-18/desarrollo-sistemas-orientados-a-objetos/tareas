"""
Implementar un programa en Python que pida al usuario que
ingrese dos números y muestre la suma, resta, división y 
multiplicación de ambos.
"""

numero_1 = float(input('Número 1: '))
numero_2 = float(input('Número 2: '))

resultados = {
    "suma": numero_1 + numero_2,
    "resta": numero_1 - numero_2,
    "division": "indefinido",
    "multiplicacion": numero_1 * numero_2
}

if numero_2 != 0:
    resultados["division"] = numero_1 / numero_2

for operacion, resultado in resultados.items():
    print(f'El resultado de la {operacion} es {resultado}')
