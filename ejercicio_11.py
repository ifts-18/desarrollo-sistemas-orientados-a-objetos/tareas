"""
Ejercicio cuentas bancarias
"""

from abc import ABC, abstractmethod
import datetime

class CuentaBancaria(ABC):
    def __init__(self, nro_cuenta: str, cbu: str, alias: str, saldo: float):
        self.__nro_cuenta = nro_cuenta
        self.__cbu = cbu
        self.__alias = alias
        self.__saldo = round(saldo, 2)
        self.__movimientos: list = []

    def consultar_saldo(self):
        return self.__saldo

    def registrar_movimiento(self, tipo_movimiento: str, monto_movimiento: float):
        fecha = datetime.datetime.now()
        monto = round(monto_movimiento, 2)
        if tipo_movimiento == 'deposito':
            self.__saldo += monto
            self.__movimientos.append((fecha, 'deposito', monto, self.__saldo))
        if tipo_movimiento == 'extraccion':
            self.__saldo -= monto
            self.__movimientos.append((fecha, 'extraccion', monto, self.__saldo))
        if tipo_movimiento == 'transferencia':
            self.__saldo -= monto
            self.__movimientos.append((fecha, 'transferencia', monto, self.__saldo))

    def depositar(self, monto_a_depositar: float):
        if monto_a_depositar > 0:
            self.registrar_movimiento('deposito', monto_a_depositar)
            return 'Operación realizada correctamente'
        return 'Error: Operación fallida'
    
    @abstractmethod
    def extraer(self, monto_a_extraer: float):
        pass
    
    @abstractmethod
    def transferir(self, monto_a_transferir: float, cuenta_destino: 'CuentaBancaria'):
        pass
    
    @property
    def nro_cuenta(self):
        return self.__nro_cuenta
    
    @nro_cuenta.setter
    def nro_cuenta(self, nro_cuenta: str):
        self.__nro_cuenta = nro_cuenta

    @property
    def cbu(self):
        return self.__cbu
    
    @cbu.setter
    def cbu(self, cbu: str):
        self.__cbu = cbu
    
    @property
    def alias(self):
        return self.__alias
    
    @alias.setter
    def alias(self, alias: str):
        self.__alias = alias

    @property
    def saldo(self):
        return self.__saldo
    
    @property
    def movimientos(self):
        return self.__movimientos
    
    @movimientos.setter
    def movimientos(self, movimiento: tuple):
        self.__movimientos.append(movimiento)

class CajaDeAhorro(CuentaBancaria):
    def __init__(
            self, 
            nro_cuenta: str, 
            cbu: str, 
            alias: str, 
            saldo: float,
            monto_limite_extracciones: float, 
            monto_limite_transferencias: float, 
            cant_extracciones_disponibles: int, 
            cant_transferencias_disponibles: int):
                super().__init__(nro_cuenta, cbu, alias, saldo)
                self.__monto_limite_extracciones = round(monto_limite_extracciones, 2)
                self.__monto_limite_transferencias = round(monto_limite_transferencias, 2)
                self.__cant_extracciones_disponibles = int(cant_extracciones_disponibles)
                self.__cant_transferencias_disponibles = int(cant_transferencias_disponibles)

    def extraer(self, monto_a_extraer: float):
        es_monto_invalido = monto_a_extraer < 0 or monto_a_extraer > self.saldo or monto_a_extraer > self.__monto_limite_extracciones or self.__cant_extracciones_disponibles <= 0
        if es_monto_invalido:
            return 'Operación fallida'
        self.registrar_movimiento('extraccion', monto_a_extraer)
        self.__cant_extracciones_disponibles -= 1
        return 'Operación realizada correctamente'
    
    def transferir(self, monto_a_transferir: float, cuenta_destino: CuentaBancaria):
        es_monto_invalido = monto_a_transferir < 0 or monto_a_transferir > self.saldo or monto_a_transferir > self.__monto_limite_transferencias or self.__cant_transferencias_disponibles <= 0
        if es_monto_invalido:
            return 'Operación fallida'
        self.registrar_movimiento('transferencia', monto_a_transferir)
        self.__cant_transferencias_disponibles -= 1
        return 'Operación realizada correctamente'
    
    @property
    def monto_limite_extracciones(self):
        return self.__monto_limite_extracciones
    
    @monto_limite_extracciones.setter
    def monto_limite_extracciones(self, monto: float):
        self.__monto_limite_extracciones = round(monto, 2)

    @property
    def monto_limite_transferencias(self):
        return self.__monto_limite_transferencias
    
    @monto_limite_transferencias.setter
    def monto_limite_transferencias(self, monto: float):
        self.__monto_limite_transferencias = round(monto, 2)
    
    @property
    def cant_extracciones_disponibles(self):
        return self.__cant_extracciones_disponibles
    
    @cant_extracciones_disponibles.setter
    def cant_extracciones_disponibles(self, cantidad: int):
        self.__cant_extracciones_disponibles = int(cantidad)
    
    @property
    def cant_transferencias_disponibles(self):
        return self.__cant_transferencias_disponibles
    
    @cant_transferencias_disponibles.setter
    def cant_transferencias_disponibles(self, cantidad: int):
        self.__cant_transferencias_disponibles = int(cantidad)

class CuentaCorriente(CuentaBancaria):
    def __init__(self, nro_cuenta: str, cbu: str, alias: str, saldo: float, monto_maximo_descubierto: float):
        super().__init__(nro_cuenta, cbu, alias, saldo)
        self.__monto_maximo_descubierto = round(monto_maximo_descubierto, 2)

    def extraer(self, monto_a_extraer: float):
        disponible_para_extraccion = self.saldo + self.__monto_maximo_descubierto
        es_monto_invalido = monto_a_extraer < 0 or monto_a_extraer > disponible_para_extraccion
        if es_monto_invalido:
            return 'Operación fallida'
        self.registrar_movimiento('extraccion', monto_a_extraer)
        return 'Operación realizada correctamente'
    
    def transferir(self, monto_a_transferir: float, cuenta_destino: CuentaBancaria):
        disponible_para_extraccion = self.__saldo + self.__monto_maximo_descubierto
        es_monto_invalido = monto_a_transferir < 0 or monto_a_transferir > disponible_para_extraccion
        if es_monto_invalido:
            return 'Operación fallida'
        self.registrar_movimiento('transferencia', monto_a_transferir)
        return 'Operación realizada correctamente'
    
    @property
    def monto_maximo_descubierto(self):
        return self.__monto_maximo_descubierto
    
    @monto_maximo_descubierto.setter
    def monto_maximo_descubierto(self, monto: float):
        self.__monto_maximo_descubierto = round(monto, 2)

class Cliente:
    def __init__(self, razon_social: str, cuit: str, tipo_persona: str, domicilio: str):
        self.__razon_social = razon_social
        self.__cuit = cuit
        self.__tipo_persona = tipo_persona
        self.__domicilio = domicilio
        self.__cuentas_bancarias: list[CuentaBancaria] = []

    def crear_caja_de_ahorro(
            self, 
            nro_cuenta: str, 
            cbu: str, 
            alias: str, 
            saldo: float,
            monto_limite_extracciones: float, 
            monto_limite_transferencias: float, 
            cant_extracciones_disponibles: int, 
            cant_transferencias_disponibles: int):
                self.__cuentas_bancarias.append(
                    CajaDeAhorro(
                        nro_cuenta, 
                        cbu, alias, 
                        saldo, 
                        monto_limite_extracciones, 
                        monto_limite_transferencias, 
                        cant_extracciones_disponibles, 
                        cant_transferencias_disponibles))
                return 'Operación realizada correctamente'
    
    def crear_cuenta_corriente(
            self,
            nro_cuenta: str, 
            cbu: str,
            alias: str,
            saldo: float,
            monto_maximo_descubierto: float):
                self.__cuentas_bancarias.append(
                    CuentaCorriente(
                        nro_cuenta,
                        cbu,
                        alias,
                        saldo,
                        monto_maximo_descubierto))
                return 'Operación realizada correctamente'

    @property
    def razon_social(self):
        return self.__razon_social
    
    @razon_social.setter
    def razon_social(self, razon_social: str):
        self.__razon_social = razon_social

    @property
    def cuit(self):
        return self.__cuit
    
    @cuit.setter
    def cuit(self, cuit: str):
        self.__cuit = cuit

    @property
    def tipo_persona(self):
        return self.__tipo_persona
    
    @tipo_persona.setter
    def tipo_persona(self, tipo_persona: str):
        self.__tipo_persona = tipo_persona

    @property
    def domicilio(self):
        return self.__domicilio
    
    @domicilio.setter
    def domicilio(self, domicilio: str):
        self.__domicilio = domicilio

    @property
    def cuentas_bancarias(self):
        return self.__cuentas_bancarias
    
    @cuentas_bancarias.setter
    def cuentas_bancarias(self, cuenta: 'CuentaBancaria'):
        self.__cuentas_bancarias.append(cuenta)

class Banco:
    def __init__(self, nombre: str, domicilio: str):
        self.__nombre = nombre
        self.__domicilio = domicilio
        self.__clientes: list[Cliente] = []

    def crear_nuevo_cliente(self, razon_social: str, cuit: str, tipo_persona: str, domicilio: str):
        nuevo_cliente = Cliente(razon_social, cuit, tipo_persona, domicilio)
        self.__clientes.append(nuevo_cliente)
        return 'Operación realizada correctamente'
    
    @property
    def nombre(self):
        return self.__nombre
    
    @nombre.setter
    def nombre(self, nombre: str):
        self.__nombre = nombre

    @property
    def domicilio(self):
        return self.__domicilio
    
    @domicilio.setter
    def domicilio(self, domicilio: str):
        self.__domicilio = domicilio

    @property
    def clientes(self):
        return self.__clientes


def main():

    # Nuevo Banco
    banco = Banco('Banco de Prueba', 'Calle 1')


    # Clientes Personas Físicas
    banco.crear_nuevo_cliente('Juan Pérez', '20360901231', 'Física', 'Av. Principal N°123, Ciudad')
    banco.crear_nuevo_cliente('María García', '27584093212', 'Física', 'Calle Secundaria N°45, Pueblo')

    # Clientes Personas Jurídicas:
    banco.crear_nuevo_cliente('Mi Empresa S.A.', '30374856001', 'Jurídica', 'Av. Corporativa N°500, Ciudad Empresarial')
    banco.crear_nuevo_cliente('Otra Empresa S.A.', '30761234001', 'Jurídica', 'Calle de Negocios N°80, Zona Industrial')

    
    # Cliente 1 - Juan Pérez
    banco.clientes[0].crear_caja_de_ahorro('4009041813521', '2850590940090418135211', 'ensalada literal ajo', 0, 15000, 15000, 10, 10)
    banco.clientes[0].crear_cuenta_corriente('4009041813522', '2850590940090418135221', 'palta agua helado', 0, 0)

    # Cliente 1 - Movimientos
    banco.clientes[0].cuentas_bancarias[0].depositar(1000)
    print(banco.clientes[0].cuentas_bancarias[0].saldo)
    banco.clientes[0].cuentas_bancarias[0].extraer(500)
    print(banco.clientes[0].cuentas_bancarias[0].saldo)
    print(banco.clientes[0].cuentas_bancarias[0].extraer(600))
    print('Movimientos Cliente 1:', banco.clientes[0].cuentas_bancarias[0].movimientos)

    
    # Cliente 2 - María García
    banco.clientes[1].crear_caja_de_ahorro('4009041813523', '2850590940090418135231', 'queso manzana pan', 0, 20000, 20000, 10, 10)
    banco.clientes[1].crear_cuenta_corriente('4009041813524', '2850590940090418135241', 'sandía uva pan', 0, 20000)

    # Cliente 2 - Movimientos
    banco.clientes[1].cuentas_bancarias[1].depositar(15000)
    print(banco.clientes[1].cuentas_bancarias[1].saldo)
    print(banco.clientes[1].cuentas_bancarias[1].extraer(49000)) # Operación fallida -monto a extraer supera saldo + descubierto
    banco.clientes[1].cuentas_bancarias[1].extraer(20000)
    print(banco.clientes[1].cuentas_bancarias[1].saldo)
    banco.clientes[1].cuentas_bancarias[1].extraer(15000)
    print(banco.clientes[1].cuentas_bancarias[1].saldo)
    print('Movimientos Cliente 2:', banco.clientes[1].cuentas_bancarias[1].movimientos)


    # Cliente 3 - Mi Empresa S.A.
    banco.clientes[2].crear_caja_de_ahorro('4009041813525', '2850590940090418135251', 'naranja banana pera', 0, 50000, 50000, 20, 20)
    banco.clientes[2].crear_cuenta_corriente('4009041813526', '2850590940090418135261', 'kiwi papaya manzana', 0, 1000000)

    # Cliente 3 - Movimientos
    banco.clientes[2].cuentas_bancarias[1].depositar(7500000)
    print(banco.clientes[2].cuentas_bancarias[1].saldo)
    print(banco.clientes[2].cuentas_bancarias[1].extraer(8400000))
    print(banco.clientes[2].cuentas_bancarias[1].saldo)
    print('Movimientos Cliente 3:', banco.clientes[2].cuentas_bancarias[1].movimientos)


    # Cliente 4 - Otra Empresa S.A.
    banco.clientes[3].crear_caja_de_ahorro('4009041813527', '2850590940090418135271', 'uva pera manzana', 0, 50000, 50000, 20, 20)
    banco.clientes[3].crear_cuenta_corriente('4009041813528', '2850590940090418135281', 'pomelo frutilla manzana', 0, 1500000)

    # Cliente 4 - Movimientos
    banco.clientes[3].cuentas_bancarias[0].depositar(1000)
    banco.clientes[3].cuentas_bancarias[0].depositar(1000)
    banco.clientes[3].cuentas_bancarias[0].depositar(1000)
    banco.clientes[3].cuentas_bancarias[0].depositar(1000)
    banco.clientes[3].cuentas_bancarias[0].depositar(1000)
    print(banco.clientes[3].cuentas_bancarias[0].saldo)
    print('Movimientos Cliente 4:', banco.clientes[3].cuentas_bancarias[0].movimientos)

main()