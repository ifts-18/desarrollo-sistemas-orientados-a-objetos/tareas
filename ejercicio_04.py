"""
Escribe un programa en Python que solicite 5 números 
enteros al usuario. El mismo debe indicar si entre 
dichos valores hay números duplicados o no, imprimiendo
por pantalla “HAY DUPLICADOS” o “SON TODOS
DISTINTOS”.
"""

numeros_ingresados = set()

for i in range(5):
    numero_ingresado = int(input('Ingrese un número entero: '))
    numeros_ingresados.add(numero_ingresado)

CANTIDAD_NUMEROS = len(numeros_ingresados)

if CANTIDAD_NUMEROS == 5:
    print('SON TODOS DISTINTOS')
else:
    print('HAY DUPLICADOS')
