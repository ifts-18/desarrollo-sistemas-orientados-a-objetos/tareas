"""
Figuras geométricas
"""

from abc import ABC, abstractmethod
import math

class FiguraGeometrica(ABC):
    def __init__(self, color_fondo: str, color_borde: str):
        self.__color_fondo = color_fondo
        self.__color_borde = color_borde

    @abstractmethod
    def area(self):
        pass

    @abstractmethod
    def perimetro(self):
        pass

    @property
    def color_fondo(self):
        return self.__color_fondo

    @color_fondo.setter
    def color_fondo(self, value):
        self.__color_fondo = value

    @property
    def color_borde(self):
        return self.__color_borde
    
    @color_borde.setter
    def color_borde(self, value):
        self.__color_borde = value


class Rectangulo(FiguraGeometrica):
    def __init__(self, color_fondo: str, color_borde: str, base: float, altura: float):
        super().__init__(color_fondo, color_borde)
        self.__base = base
        self.__altura = altura

    def area(self):
        return self.__base * self.__altura

    def perimetro(self):
        return self.__base * 2 + self.__altura * 2

    @property
    def base(self):
        return self.__base
    
    @base.setter
    def base(self, value):
        self.__base = value

    @property
    def altura(self):
        return self.__altura

    @altura.setter
    def altura(self, value):
        self.__altura = value

class Circulo(FiguraGeometrica):
    def __init__(self, color_fondo: str, color_borde: str, radio: float):
        super().__init__(color_fondo, color_borde)
        self.__radio = radio

    def area(self):
        return 2 * self.__radio * math.pi 

    def perimetro(self):
        return 2 * self.__radio

    @property
    def radio(self):
        return self.__radio

    @radio.setter
    def radio(self, value):
        self.__radio = value

class Triangulo(FiguraGeometrica):
    def __init__(self, color_fondo: str, color_borde: str, base: float, altura: float):
        super().__init__(self, color_fondo, color_borde)
        self.__base = base
        self.__altura = altura

    def area(self):
        return (self.__base * self.__altura) / 2

    def perimetro(self):
        pass

    @property
    def base(self):
        return self.__base

    @base.setter
    def base(self, value):
        self.__base = value

    @property
    def altura(self):
        return self.__altura
    
    @altura.setter
    def altura(self, value):
        self.__altura = value

def main():
    rectangulo = Rectangulo('negro', 'blanco', 10, 20)
    print(rectangulo.area())

main()
