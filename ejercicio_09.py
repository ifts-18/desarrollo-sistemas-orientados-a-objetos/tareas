"""
Ejercicio POO con encapsulamiento y uso de atributos de instancia y de clase

Crear una clase llamada Password con las siguientes condiciones:

• Dos atributos de clase privados:
  1. LONGITUD. Cuyo valor sea 8 (numérico).
  2. CARACTERES_VALIDOS. Cuyo valor sea
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%
    &+" (string)

• Dos atributos de instancia privados:
  1. longitud. Por defecto, será igual al valor del atributo de clase LONGITUD. Y su valor no
  podrá ser inferior a 6 caracteres ni mayor a 15 caracteres.
  2. contraseña. Su valor aleatorio deberá ser asignado en el método generarPassword().

• Un método inicializador de instancias, con el parámetro longitud cuyo valor se asignará al
atributo de instancia. Generará una contraseña aleatoria con esa longitud invocando al
método generarPassword().

• Dos métodos de instancia públicos, cuya implementación deberá ser:
  1. esFuerte(): devuelve un booleano si es fuerte o no. Para que sea fuerte debe tener
  más de 1 mayúscula, 1 carácter especial, más de 1 minúscula y más de 1 números.
  2. generarPassword(): genera la contraseña del objeto cuyo valor de tipo string tendrá
  una longitud igual al valor del atributo de instancia “longitud”. Para la generación de la
  clave puede usar los métodos random.choice() y string.join() de Pythniceon.

• Incluir métodos públicos que permitan obtener y asignar valores (getters y setters) a los
atributos de instancia privados.

• Sobreescribir el método de instancia __str__(), para que retorne la clave generada y el
valor booleano que devuelve el método “es_fuerte()”.
Luego, agregar sentencias de código Python que permitan:
nice
• Crear una lista de objetos de tipo Password.

• Crear instancias de Password y agregarlas a la lista. Para cada objeto, se debe ingresar la
longitud de la clave por teclado. Si el valor ingresado es cero, no se pasará ningún valor
como argumento al método inicializador.

• Mostrar cada una de las contraseñas creadas y si es o no fuerte (usar un bucle). Para ello,
usar este simple formato:
contraseña1 - valor_booleano1
contraseña2 - valor_bololeano2
"""

import random

class Password:
    __LONGITUD_POR_DEFECTO = 8
    __CARACTERES_VALIDOS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%&+'

    def __init__(self, longitud: int):
        self.__longitud = self.__ingresar_longitud(longitud)
        self.__password = ''

    def __ingresar_longitud(self, longitud: int):
        if longitud < 6 or longitud > 15:
            return self.__LONGITUD_POR_DEFECTO
        return longitud

    def generar_password(self):
        "Setter password"
        password_generada = ''
        for _ in range(self.__longitud):
            caracter = random.choice(self.__CARACTERES_VALIDOS)
            password_generada += caracter
        self.__password = password_generada

    def get_password(self):
        "Getter password"
        return self.__password

    def es_fuerte(self):
        "Es fuerte si tiene: más de 1 mayus, más de 1 minus, 1 caracter especial, más de 1 número"
        mayusculas = 0
        minusculas = 0
        caracteres_especiales = 0
        numeros = 0
        for caracter in self.get_password():
            if caracter.isupper():
                mayusculas += 1
            if caracter.islower():
                minusculas += 1
            if not caracter.isalnum():
                caracteres_especiales += 1
            if caracter.isnumeric():
                numeros += 1
            cumple_condiciones = mayusculas > 1 and caracteres_especiales > 0 and minusculas > 1 and numeros > 1
            if cumple_condiciones:
                return True
        return False

    def set_longitud(self, longitud: int):
        "Setter longitud"
        self.__longitud = self.__ingresar_longitud(longitud)

    def get_longitud(self):
        "Getter longitud"
        return self.__longitud

    def __str__(self):
        return f'{self.get_password()} - {self.es_fuerte()}'

if __name__ == '__main__':

    lista_passwords: Password = []
    for _ in range(4):
        nueva_password = Password(int(input('Ingrese una longitud entre 6 y 15 caracteres: ')))
        nueva_password.generar_password()
        lista_passwords.append(nueva_password)

    for password in lista_passwords:
        print(password)
