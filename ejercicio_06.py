"""
Usando la función del ejercicio anterior, escribir un programa
que pida al usuario dos intervalos expresados en horas, minutos 
y segundos, sume sus duraciones, y muestre por pantalla la
duración total en horas, minutos y segundos.
"""

from ejercicio_05 import calcular_duracion_en_segundos

def solicitar_intervalo_usuario():
    return {
        "horas": int(input('Horas: ')),
        "minutos": int(input('Minutos: ')),
        "segundos": int(input('Segundos: '))
    }

def convertir_segundos_en_horas_minutos_segundos(segundos):
    horas_obtenidas = segundos // 3600
    segundos_restantes = segundos - horas_obtenidas * 3600
    minutos_obtenidos = segundos_restantes // 60
    segundos_restantes = segundos_restantes - minutos_obtenidos * 60
    return {
        "horas": horas_obtenidas,
        "minutos": minutos_obtenidos,
        "segundos": segundos_restantes
    }

print('Ingrese el primer intervalo:')
intervalo_1 = solicitar_intervalo_usuario()
segundos_totales_intervalo_1 = calcular_duracion_en_segundos(**intervalo_1)

print('Ingrese el segundo intervalo:')
intervalo_2 = solicitar_intervalo_usuario()
segundos_totales_intervalo_2 = calcular_duracion_en_segundos(**intervalo_2)

segundos_totales = segundos_totales_intervalo_1 + segundos_totales_intervalo_2
duracion_total = convertir_segundos_en_horas_minutos_segundos(segundos_totales)

print(
    'Duración total de ambos intervalos: ', 
    f'{duracion_total["horas"]} horas, ',
    f'{duracion_total["minutos"]} minutos '
    f'y {duracion_total["segundos"]} segundos'
)
