"""
Ejercicio de Manejo de Bases de Datos con Python

Desarrollar una aplicación Python que se conecte a una base de datos SQLite, llamada
empleados. Que contiene una tabla también llamada empleados.
La estructura de la tabla empleados es la siguiente:
• id int pk ai not null,
• nro_legajo int NOT NULL UNIQUE,
• dni int NOT NULL UNIQUE,
• nombre text NOT NULL,
• apellido text NOT NULL,
• area text NOT NULL

La aplicación debe permitir crear la tabla. La creación de la tabla debe estar en una
función o método llamado crearTabla.
El programa debe solicitar al usuario que ingrese por consola una de las siguientes
opciones:
• Opcion 1 Insertar un registro de empleado.
• Opcion 2 Selecionar un registro de empleado a partir de su numero DNI.
• Opcion 3 Selecionar todos los empleados o los registros de la tabla.
• Opcion 4 Modificar el area de un empleado en función de su numero de legajo.
• Opcion 5 Eliminar un empleado a partir del numero de legajo.
• Opcion 6 Finalizar.

Cada opción se tiene que ingresar por teclado. Cada una de las sentencias que van a
permitir ejecutar cada opción debe estar en una función por separado (salvo la opción
nro 6). Es decir, la opción 1 “insertar un registro de empleado” tiene su propia función.
La conexión a la Base de Datos también debe estar en una función por separado, al
igual que la creación de la tabla es otra función.
Luego de realizar acciones de modificación de datos INSERT DELETE UPDATE se debe
cerrar la conexión.

Notación: ai auto incremental , pk primary key
"""

import sqlite3

TEXTO_ENCABEZADO = '''
|==========================================================|
|---------------| BASE DE DATOS: Empleados |---------------|
|==========================================================|
'''

TEXTO_OPCIONES = '''
|==========================================================|
|-----------------| Opciones disponibles |-----------------|
|----------------------------------------------------------|
| - Opción 1: Insertar nuevo registro de empleado          |
| - Opción 2: Buscar empleado por Nro de DNI               |
| - Opción 3: Seleccionar todos los registros              |
| - Opción 4: Modificar área de un empleado                |
| - Opción 5: Eliminar un registro de empleado             |
| - Opción 6: Mostrar menú                                 |
| - Opción 7: Finalizar                                    |
|----------------------------------------------------------|
'''

ERROR_GENERICO = '# Operación fallida, intente nuevamente'

class BaseDeDatos:
    def __init__(self, nombre_bd: str):
        self.__nombre_bd = nombre_bd
        self.__bd: sqlite3.Connection
        self.__cursor: sqlite3.Cursor
        self.__conexion_establecida = False

    def iniciar_conexion(self):
        self.__bd = sqlite3.connect(f'{self.__nombre_bd}.db')
        self.__conexion_establecida = True
        self.__cursor = self.__bd.cursor()
        print(F'# Base de datos {self.__nombre_bd}: CONEXIÓN ESTABLECIDA')

    def finalizar_conexion(self):
        try:
            self.__bd.close()
            self.__conexion_establecida = False
            print(f'# Base de datos {self.__nombre_bd}: CONEXIÓN FINALIZADA')
        except:
            print(ERROR_GENERICO)

    def conexion_establecida(self):
        return self.__conexion_establecida

    def crear_tabla(self, nombre_tabla: str):
        nombre = nombre_tabla
        consulta = f'''
            CREATE TABLE IF NOT EXISTS {nombre} (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                nro_legajo INTEGER NOT NULL UNIQUE,
                dni INTEGER NOT NULL UNIQUE,
                nombre TEXT NOT NULL,
                apellido TEXT NOT NULL,
                area TEXT NOT NULL)
        '''
        try:
            self.__cursor.execute(consulta)
        except:
            print(ERROR_GENERICO)

    def insertar_registro(self, nro_legajo: str, dni: int, nombre: str, apellido: str, area: str):
        consulta = '''
            INSERT INTO empleados (nro_legajo, dni, nombre, apellido, area) 
            values (?, ?, ?, ?, ?)
        '''
        try:
            self.__cursor.execute(consulta, [nro_legajo, dni, nombre, apellido, area])
            self.__guardar_registro()
        except sqlite3.IntegrityError:
            print(f'# Nro de legajo {nro_legajo} existente, registro no ingresado')
        except:
            print(ERROR_GENERICO)

    def __guardar_registro(self):
        self.__bd.commit()

    def seleccionar_registro_por_dni(self, dni: int):
        consulta = 'SELECT * FROM empleados WHERE dni=?'
        try:
            respuesta = self.__cursor.execute(consulta, [dni]).fetchone()
            if respuesta is None:
                return '# Registro no encontrado'
            return respuesta
        except:
            print(ERROR_GENERICO)
    
    def seleccionar_todos_los_registros(self):
        consulta = 'SELECT * FROM empleados'
        try:
            respuesta = self.__cursor.execute(consulta).fetchall()
            return respuesta
        except:
            print(ERROR_GENERICO)
    
    def modificar_area_empleado(self, area: str, nro_legajo: str):
        consulta = 'UPDATE empleados SET area=? WHERE nro_legajo=?'
        try:
            self.__cursor.execute(consulta, [area, nro_legajo])
            self.__guardar_registro()
        except:
            print(ERROR_GENERICO)

    def eliminar_registro_empleado(self, nro_legajo: str):
        consulta = 'DELETE FROM empleados WHERE nro_legajo=?'
        try:
            self.__cursor.execute(consulta, [nro_legajo])
            self.__guardar_registro()
        except:
            print(ERROR_GENERICO)

def mostrar_encabezado():
    print(TEXTO_ENCABEZADO)

def mostrar_opciones():
    print(TEXTO_OPCIONES)

def generar_plantilla(respuesta):
    plantilla = f'''\n#____________________| Nro legajo: {respuesta[1]} |____________________#
    \n|- DNI: {respuesta[2]}\n|- Nombre: {respuesta[3]}\n|- Apellido: {respuesta[4]}\n|- Área: {respuesta[5]}
    '''
    return plantilla

def ejecutar_opcion_ingresada(bd: BaseDeDatos, opcion: int):
    match opcion:
        case 1:
            print('\n|=========| Insertar nuevo registro de empleado |==========|')
            nro_legajo = int(input('# Nro de legajo: '))
            dni = int(input('# Nro de DNI: '))
            nombre = str(input('# Nombre: '))
            apellido = str(input('# Apellido: '))
            area = str(input('# Área: '))
            bd.insertar_registro(nro_legajo, dni, nombre, apellido, area)
        
        case 2:
            print('\n|============| Buscar empleado por Nro de DNI |============|')
            dni = int(input('# Nro de DNI: '))
            respuesta = bd.seleccionar_registro_por_dni(dni)
            if respuesta == '# Registro no encontrado':
                print(respuesta)
                return
            print(generar_plantilla(respuesta))
        
        case 3:
            print('\n|===========| Seleccionar todos los registros |============|')
            registros = bd.seleccionar_todos_los_registros()
            if registros is not None:
                cantidad = len(registros)
                print(f'# Registros existentes: {cantidad}')
                for registro in registros:
                    print(generar_plantilla(registro))
        
        case 4:
            print('\n|============| Modificar área de un empleado |=============|')
            nro_legajo = int(input('# Ingrese el nro de legajo del empleado: '))
            nueva_area = str(input('# Ingrese el nuevo valor del área del empleado: '))
            bd.modificar_area_empleado(nueva_area, nro_legajo)
        
        case 5:
            print('\n|==========| Eliminar un registro de empleado |============|')
            nro_legajo = int(input('# Nro de legajo: '))
            bd.eliminar_registro_empleado(nro_legajo)
        
        case 6:
            mostrar_opciones()

        case 7:
            bd.finalizar_conexion()

        case _:
            print('# Opción inexistente')

def ejecutar_programa_en_terminal():
    bd_empleados = BaseDeDatos('empleados')
    bd_empleados.iniciar_conexion()
    bd_empleados.crear_tabla('empleados')

    mostrar_encabezado()
    mostrar_opciones()

    while bd_empleados.conexion_establecida():
        try:
            print('\n|============| Ingresar una opción del 1 al 7 |============|')
            opcion_ingresada = int(input('# Opción ingresada: '))
            ejecutar_opcion_ingresada(bd_empleados, opcion_ingresada)
        except ValueError:
            print('# Opción inválida')

ejecutar_programa_en_terminal()