"""
Escribir un programa en Python que pida al usuario que ingrese
las medidas de la base y la altura de un rectángulo y muestre:

1. El perímetro del rectángulo
2. El área del rectángulo
"""

base = int(input('Ingrese la medida de la base en cm: '))
altura = int(input('Ingrese la altura en cm: '))

if(base <= 0 or altura <= 0):
    print('Alguno de los valores ingresados es incorrecto')
else:
    perimetro = base * 2 + altura * 2
    area = base * altura
    print('El perímetro del rectángulo es:', perimetro)
    print('El área del rectángulo es:', area)
