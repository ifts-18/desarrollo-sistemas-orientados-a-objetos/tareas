class Mando:
    def siguiente_canal(self):
        pass
    def canal_anterior(self):
        pass
    def subir_volumen(self):
        pass
    def bajar_volumen(self):
        pass

class MandoSamsung(Mando):
    def subir_volumen(self):
        print("Samsung->Subir")
    
class MandoLG(Mando):
    def siguiente_canal(self):
        print("LG->Siguiente")
    def canal_anterior(self):
        print("LG->Anterior")
    def subir_volumen(self):
        print("LG->Subir")
    def bajar_volumen(self):
        print("LG->Bajar")

mando_sansung = MandoSamsung()
mando_sansung.subir_volumen()

mando = Mando()
mando.subir_volumen()