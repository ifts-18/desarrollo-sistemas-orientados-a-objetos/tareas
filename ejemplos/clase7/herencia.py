class Animal():
    def __init__(self, especie, edad):
        print("Soy el constructor de Animal")
        self.especie = especie
        self.edad = edad
    
    def comer(self):
        pass

    def hablar(self):
        pass

    def moverse(self):
        pass

    def describeme(self):
        return f"Soy un Animal del tipo {type(self)}"

class Perro(Animal):
    def __init__(self, especie, edad, dueño):
        print("Soy el constructor de Perro")

        super().__init__(especie, edad)
        self.dueño = dueño

#print("valor de name",__name__)
if __name__ == "__main__":
    mi_perro = Perro('mamífero', 7, 'Luis')
    print(mi_perro.especie)
    print(mi_perro.edad)
    print(mi_perro.dueño)
    print(mi_perro.describeme())
