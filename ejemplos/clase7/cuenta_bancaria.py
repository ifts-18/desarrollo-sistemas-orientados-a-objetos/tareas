from abc import ABC,abstractmethod
from datetime import datetime



class CuentaBancaria(ABC):
    
    def _init_(self,nro_cuenta,cbu,alias,saldo=0):
        self.nro_cuenta=nro_cuenta
        self.cbu=cbu
        self.alias=alias
        self.movimientos=[]
        self.saldo=saldo

    @property
    def nro_cuenta(self):
        return self.__nro_cuenta
    
    @nro_cuenta.setter
    def nro_cuenta(self,valor):
        self.__nro_cuenta=valor

    @property
    def cbu(self,):
        return self.__cbu
    
    @cbu.setter
    def cbu(self,valor):
        self.__cbu=valor

    @property
    def alias(self):
        return self.__alias
    
    @alias.setter
    def alias(self,valor):
        self.__alias=valor

    @property
    def movimientos(self):
        return self.__movimientos
    
    @movimientos.setter
    def movimientos(self,valor):
        self.__movimientos=valor
    

    @property
    def saldo(self):
        return self.__saldo
    
    @saldo.setter
    def saldo(self,valor):
        self.__saldo=valor
    
        
 # METODOS DE INSTANCIA
        
    def consultar_saldo(self):
        return self.saldo
    
    def depositar(self,monto_a_depositar):
        if monto_a_depositar>0:
            self.saldo+=monto_a_depositar
            self.movimientos.append((datetime.now().date(),"deposito",monto_a_depositar,self.saldo))
            return True
        else:
            return False
    
    @abstractmethod
    def extraer(self,monto_a_extraer):
        pass

    @abstractmethod
    def transferir(self,transferir):
        pass
        
        
class CajaDeAhorro(CuentaBancaria):
    def _init_(self,nro_cuenta,cbu,alias,saldo,monto_limite_extracciones,monto_limite_transferencias,cant_extracciones_disponibles,cant_transferencias_disponibles):
        super()._init_(self,nro_cuenta,cbu,alias,saldo)
        self.monto_limite_extraciones=monto_limite_extracciones