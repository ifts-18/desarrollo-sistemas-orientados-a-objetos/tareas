from abc import abstractmethod
from abc import ABCMeta

class Mando(metaclass=ABCMeta):
    
    @abstractmethod
    def siguiente_canal(self):
        pass

    @abstractmethod
    def canal_anterior(self):
        pass

    def subir_volumen(self):
        print("Mando->subir_volumen")

    @abstractmethod
    def bajar_volumen(self):
        print("Mando->bajar_volumen")

class MandoSamsung(Mando):
    def siguiente_canal(self):
        print("Samsung->siguiente_canal")
    def canal_anterior(self):
        print("Samsung->canal_anterior")
    def subir_volumen(self):
        print("Samsung->subir_volumen")
    def bajar_volumen(self):
        super().bajar_volumen()
        print("Samsung->bajar_volumen")

mando = Mando()

mando_samsung = MandoSamsung()
mando_samsung.bajar_volumen()