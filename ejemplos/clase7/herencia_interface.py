from abc import ABC, abstractmethod

class ISerVivo(ABC):
    @abstractmethod
    def comer(self):
        pass

    @abstractmethod
    def hablar(self):
        pass

    @abstractmethod
    def moverse(self):
        pass

class Animal():
    def __init__(self, especie, edad):
        print("Soy el constructor de Animal")
        self.especie = especie
        self.edad = edad

    def describeme(self):
        return f"Soy un Animal del tipo {type(self)}"

class Perro(Animal, ISerVivo):
    def __init__(self, especie, edad, dueño):
        print("Soy el constructor de Perro")

        super().__init__(especie, edad)
        self.dueño = dueño
    
    def comer(self):
        return "Me encantan los huesos!"

    def hablar(self):
        return "Guau!"
    
    def moverse(self):
        return "Puedo caminar, correr y saltar!"

class Gato(Animal, ISerVivo):
    def __init__(self, especie, edad):
        print("Soy el constructor de Gato")
        super().__init__(especie, edad)

    def comer(self):
        return "Me encantan el balanceado!"

    def hablar(self):
        return "Miau!"
    
    def moverse(self):
        return "Puedo caminar, correr y saltar!"

#print("valor de name",__name__)
if __name__ == "__main__":
    mi_perro = Perro('mamífero', 7, 'Luis')
    print(mi_perro.especie)
    print(mi_perro.edad)
    print(mi_perro.dueño)
    print(mi_perro.describeme())

    mi_gato = Gato("mamifero",2)

    #mi_animal = Animal()
    
