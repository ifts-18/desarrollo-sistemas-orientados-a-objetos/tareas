from abc import ABCMeta, abstractmethod

class Mando(metaclass=ABCMeta):
    @abstractmethod
    def subir_volumen(self):
        pass

    @abstractmethod
    def siguiente_canal(self):
        pass

class MandoSamsung(Mando):
    def subir_volumen(self):
        print("Samsung->Subir volumen")
    
    def siguiente_canal(self):
        print("Samsung->siguiente canal")

class MandoLG(Mando):
    def subir_volumen(self):
        print("LG->Subir volumen")
    
    def siguiente_canal(self):
        print("LG->siguiente canal")
    

if __name__ == "__main__":
    #obj_mando = Mando()
    #obj_mando.subir_volumen()

    obj_samsung = MandoSamsung()
    obj_samsung.subir_volumen()

    obj_lg = MandoLG()
    obj_lg.subir_volumen()
    obj_lg.siguiente_canal()

    print(isinstance(obj_samsung, object))
    print(type(obj_lg))
    print(issubclass(MandoSamsung, object))

    for control in MandoSamsung(), MandoLG():
        control.subir_volumen()
        control.siguiente_canal()