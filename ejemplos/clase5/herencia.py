class Instrumento(object): 
    def __init__(self, precio):
        self.precio = precio 
    
    def tocar(self): 
        print("Estamos haciendo música")

    def romper(self): 
        print("Eso lo pagas tú!")
        print("Son $", self.precio)

class Bateria(Instrumento): 
    def __init__(self, precio):
        pass

class Guitarra(Instrumento): 
    pass

bat = Bateria(550000.00)
bat.tocar()

#isinstance
print("bat es una instancia de Bateria?",isinstance(bat,Bateria))

#issubclass
print("Bateria es una subclase de Instrumento?",issubclass(Bateria,Instrumento))