'''
Crear una clase llamada Persona.
Con los siguientes atributos:
nombre
edad
dni
Y los siguientes métodos:
mostrar_edad(): retorna la edad de la persona
es_mayor_edad(): retorna True si edad es mayor o igual a 18, o False si es menor a 
El método constructor de la clase debe recibir y asignar los valores a cada uno de los atributos de la clase.
'''

class Persona(object):
    #Atributos de clase
    RAZA = "Homo sapiens"

    #Método inicializador de atributos de instancias
    def __init__(self, nombre:str, edad:int, dni:int):
        print("Inicializando los atributos del objeto")
        #Atributos de instancia
        self.nombre = nombre
        self.__edad = edad
        self.dni = dni
        #Persona.RAZA = "Otra raza que pertenece a la Clase"
        #self.RAZA = "Otra raza que pertenece al objeto"

    #Método de instancia de tipo getter para el atributo edad
    '''Los métodos de instancia:
    Reciben como parámetro de entrada self que hace referencia a la instancia que llama al método. También pueden recibir otros argumentos como entrada.
    Pueden acceder y modificar los atributos del objeto.
    Pueden acceder a otros métodos.'''
    def mostar_edad(self) -> int:
        return self.__edad
    
    #Método de instancia de tipo setter para el atributo edad
    def asignar_edad(self, edad: int):
        if edad > 0 and edad < 100:
            self.__edad = edad
        else:
            raise ValueError("El valor de edad debe ser mayor a cero.")

    #Método de instancia que evalúa el valor del atributo edad y
    #determina si es mayor de edad o no.
    def es_mayor_edad(self) -> bool:
        '''Retorna True si edad es mayor o igual  a 18, False en caso contrario'''
        return self.__edad >= 18
    
    #Método de instancia de tipo setter para un nuevo atributo: loc_nacimiento    
    def localidad_nacimiento(self, valor):
        '''Este nuevo atributo se crea y se agrega a la lista de atributos del objeto que invoca a este método'''
        self.loc_nacimiento = valor
    
    #Método de instancia que sobreescribe la implementación heredada de object
    def __str__(self):
        #Redefine la representación en cadena de texto del estado del objeto
        return "Nombre: " + self.nombre + ", DNI: " + str(self.dni) \
            + ", Edad: " + str(self.__edad)

if __name__ == "__main__":
    p1 = Persona("Juan",17,46747012)
    #p1.__edad = 290
    p1.asignar_edad(29)
    p1.localidad_nacimiento("San Nicolás")
    p1.loc_provincia = "Buenos Aires"
    
    #Eliminar objetos o atributos
    #del(p1.loc_provincia)
    
    #Invocación de métodos de instancia para mostrar los valores de los atributos del objeto
    print("Nombre:",p1.nombre)
    print("Edad:",p1.mostar_edad())
    print("Es mayor de edad:",p1.es_mayor_edad())
    print("Creamos la 1er instancia de Persona:",p1.__dict__)

    #Creación de un nuevo objeto Persona
    p2 = Persona("María",21,41052331)
    print("Creamos la 2da instancia de Persona:",p2.__str__())
    print("Atributo de clase Persona:",Persona.RAZA)
    
    
    #isinstance
    print("p1 es una instancia de Persona?",isinstance(p1,Persona))
    
    #Funcion de Python utilizada con clases en relación de Herencia
    #issubclass
    print("Persona es una subclase de object?",issubclass(Persona,object))