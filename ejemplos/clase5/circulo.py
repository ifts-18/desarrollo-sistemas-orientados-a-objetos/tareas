class Circulo:
    # Atributo de clase
    PI = 3.141592
    
    def __init__(self, radio):
        # Atributo de instancia
        self.radio = radio
    
    def area(self):
        return Circulo.PI * self.radio ** 2

c1 = Circulo(2)
print(f"Area de un círculo de radio {c1.radio}: {c1.area()}")
c2 = Circulo(3)
print(f"Area de un círculo de radio {c2.radio}: {c2.area()}")
print("c1.PI:", c1.PI)
print("Circulo.PI:", Circulo.PI)
#Atributo dinámico agregado al objeto c1
c1.PI = 14
#Nuevo atributo dinámico agregado al objeto c1
c1.color_fondo = "gris"
print(c1.color_fondo)
print("objeto c1",vars(c1))
print("objeto c2",c2.__dict__)
#print(Circulo.radio)