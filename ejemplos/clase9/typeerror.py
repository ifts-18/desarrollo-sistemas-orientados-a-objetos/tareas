def calculo(a,b):
    try:
        if (a < b):
            c = a * 2 + b
            print("El resultado es:", c)
        else:
            c = a + b * 4
            print("El resultado es:", c)
    except TypeError as e:
        print("Se produjo una excepcion de tipo de dato.")
    else:
        print("No se produjo una excepcion")
    finally:
        print("El bloque finally siempre se ejecuta")

calculo(48,"5")
calculo(428,5)