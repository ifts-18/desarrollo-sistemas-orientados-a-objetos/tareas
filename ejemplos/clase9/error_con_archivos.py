import os

print("Mi directorio de trabajo es:", os.getcwd())
try:
  mi_archivo = open(os.getcwd() + "/clase9/receta.txt", mode="r", encoding="utf-8")
except FileNotFoundError:
  print("Lo siento, el archivo no existe.")

#contenido = mi_archivo.read()
#print(contenido)
i = 0
for linea in mi_archivo:
  i += 1
  print(f"Linea nro {i}: {linea}")