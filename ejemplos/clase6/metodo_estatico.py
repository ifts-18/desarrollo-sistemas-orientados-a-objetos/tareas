import math

class Circulo:
    # Atributo de clase
    __PI = 3.141592
    
    #Método de clase
    @classmethod
    def area(cls, radio):
        return cls.__PI * math.pow(radio,2)
    
    #Método estático
    @staticmethod
    def dibujar(ancho_linea):
        print(f"Soy un método estático y estoy dibujando el Círculo con un ancho de línea de {ancho_linea} mm")
    
#invocando al método de clase
resultado = Circulo.area(10)
print(resultado)

#invocando al método estático
Circulo.dibujar(3)
obj = Circulo()
obj.dibujar(10)
