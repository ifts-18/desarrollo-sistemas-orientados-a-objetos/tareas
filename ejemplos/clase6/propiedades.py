# Uso del decorador @property
class Celsius:
    def __init__(self, temperature=0):
        self.temperature = temperature

    @property
    def temperature(self):
        print("Obteniendo valor...")
        return self.__temperature

    @temperature.setter
    def temperature(self, value):
        if value < -273.15:
            print("La temperatura por debajo de -273° no es posible")
        else:
            print("Asignando valor...")
            self.__temperature = value
    
    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32

# creamos un objeto
human = Celsius(37)
print(human.temperature)
human.temperature = 42
print(human.__dict__)
'''print(human.to_fahrenheit())
human.temperature = -300
print(human.__dict__)'''
obj_muy_frio = Celsius(-300)
print(obj_muy_frio.__dict__)