import math

class Circulo:
    # Atributo de clase
    __PI = 3.141592

    #Método de clase
    @classmethod
    def area(cls, radio):
        return cls.__PI * math.pow(radio,2)
    
    #Metodo de instancia
    def met_instancia(self):
        print("valor del atributo de clase PI:",self.__PI)
        print("Soy un metodo de instancia")

resultado = Circulo.area(10)
print(resultado)
#Circulo.met_instancia()

obj = Circulo()
result = obj.area(5)
print(result)
obj.met_instancia()