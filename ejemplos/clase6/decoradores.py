def funcion_decorador(funcion_a_decorar):
    def nueva_funcionalidad():
        print("Soy la función decoradora. Agrego funcionalidad a una función.")
        funcion_a_decorar()
        print("Mostrando algo luego de invocar a la funcion")
    return nueva_funcionalidad

@funcion_decorador
def funcion_normal():
    print("Soy una función normal.")

funcion_normal()