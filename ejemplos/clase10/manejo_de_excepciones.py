from errores import MiError

def es_nro_primo(num):
    for n in range(2, num):
        if num % n == 0:
            raise MiError(f"{num} no es un nro primo")
    return "Es un nro primo"

while True:
    try:
        nro = int(input("Ingrese un número: "))
        break
    except ValueError as e:
        print("Debe ingresar un valor numérico!")

try:
    print(es_nro_primo(nro))
except MiError as e:
    print("Se lanzó la excepcion MiError.",e)

print("Luego continuo con la ejecución")