class NotIntError(Exception):
    def __init__(self, value, message="Este módulo funciona únicamente con números enteros. Lo siento!"):
        super().__init__(message)
        self.value = value
        self.message = message
    
    """ def __str__(self):
        return f"{self.value} -> {self.message}" """
        
def solo_enteros(valores):
    for value in valores:
        if not isinstance(value, int):
            raise NotIntError(value)
            #print(f"El valor {value} no es nro entero")
            #return
    print(valores)
    print("Todos los valores de la lista son números enteros!")

lista = [4, 7, "4", 9]
try:
    solo_enteros(lista)
except NotIntError as exc:
    print("Se genero una Exception de tipo NotIntError.",exc)
except Exception as exc:
    print("Se genero una Exception.",exc)

print("Continuamos con la ejecucion")