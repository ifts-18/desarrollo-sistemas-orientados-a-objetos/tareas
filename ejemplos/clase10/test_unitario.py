import unittest

def es_nro_primo(num):
    if num < 0:
        return "Los números negativos no son permitidos"
    for n in range(2, num):
        if num % n == 0:
            return False
    return True

class TestUtils(unittest.TestCase):
    def test_is_prime(self):
        self.assertFalse(es_nro_primo(4))
        self.assertTrue(es_nro_primo(2))
        self.assertTrue(es_nro_primo(3))
        self.assertFalse(es_nro_primo(21))
        self.assertFalse(es_nro_primo(10))
        self.assertTrue(es_nro_primo(7))
        self.assertEqual(es_nro_primo(-3),
                         "Los números negativos no son permitidos")

if __name__ == '__main__':
    unittest.main()