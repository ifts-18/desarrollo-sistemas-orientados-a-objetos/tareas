def numeros_positivos(valor):
    assert int(valor) > 0
    print("El nro ingresado es mayor a cero")

valor = input("Ingrese un valor numérico: ")
try:
    numeros_positivos(valor)
except AssertionError as e:
    print("Atrapo la excepcion", e)

print("Continuamos")