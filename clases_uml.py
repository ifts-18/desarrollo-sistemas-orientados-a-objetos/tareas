"""
Ejercicio de Diagrama de Clases UML

Construir un Diagrama de Clases UML identificando las clases, 
sus atributos, susmétodos y las relaciones entre las clases, 
según el siguiente escenario:

Se tienen clientes de los que se guarda un número de cliente, 
nombre, apellido, teléfono y correo electrónico.

Los clientes realizan pedidos. Cada pedido tiene un número de 
pedido, una fecha asociada, un estado (sin confirmar o confirmado) 
y el cliente que realizó el pedido. Cada pedido tiene varias líneas 
de detalle, cada una con una cantidad y un artículo.

Los artículos tienen un código, un nombre, una categoría y un precio.
"""

import datetime

class Cliente:
    "Clase Cliente"
    def __init__(self, id_cliente, nombre, apellido, telefono, email):
        self.__id_cliente = id_cliente
        self.__nombre = nombre
        self.__apellido = apellido
        self.__telefono = telefono
        self.__email = email

    def actualizar_telefono(self, telefono):
        "Actualiza teléfono"
        self.__telefono = telefono

    def actualizar_email(self, email):
        "Actualiza email"
        self.__email = email

    def obtener_datos(self):
        "Retorna nombre, apellido, telefono y email"
        return {
            "id_cliente": self.__id_cliente, 
            "nombre": self.__nombre,
            "apellido": self.__apellido,
            "telefono": self.__telefono,
            "email": self.__email
        }

class Pedido:
    "Clase Pedido"
    def __init__(self, id_pedido, cliente):
        self.__id_pedido: int = id_pedido
        self.__fecha: str = datetime.datetime.now().strftime('%x')
        self.__estado: str = 'Pedido sin confirmar'
        self.__cliente: dict = cliente
        self.__lineas_detalle: list = []

    def asociar_cliente(self):
        self.__cliente

    def obtener_datos(self):
        "Retorna id, fecha, estado, cliente, lineas detalle"
        return {
            "id_pedido": self.__id_pedido,
            "fecha": self.__fecha,
            "estado": self.__estado,
            "cliente": self.__cliente,
            "lineas_detalle": self.__lineas_detalle
        }

    def actualizar_estado(self):
        "Actualiza el estado del pedido"
        self.__estado = 'Pedido confirmado'

    def agregar_articulo(self, articulo: dict):
        "Agrega linea detalle"
        if articulo.length >= 1:
            self.__lineas_detalle.append(articulo)

class Articulo:
    "Clase Articulo"
    def __init__(self, id_articulo, nombre, categoria, precio):
        self.id_articulo: int = id_articulo
        self.__nombra: str = nombre
        self.categoria: str = categoria
        self.precio: float = precio

    def actualizar_precio(self, precio):
        self.precio = precio

    def obtener_datos(self):
        return {
            "id": self.id_articulo,
            "nombre": self.__nombra,
            "categoria": self.categoria,
            "precio": self.precio
        }

matias = Cliente(1, 'Matías', 'Castro', '1122334455', 'email@email.com')
pedido = Pedido(1, matias.obtener_datos())

print('Cliente:', matias)
print('Pedido:', pedido.obtener_pedido())
