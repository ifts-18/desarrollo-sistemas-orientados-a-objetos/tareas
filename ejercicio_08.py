"""
Crear una clase llamada ListaDeTareas con los siguientes atributos y métodos:

• Atributo de instancia privado “lista_tareas” de tipo list.
• Método de instancia público “agregarTarea(tarea)”, que recibe como argumento
una tarea que debe ser agregada a la lista de tareas (atributo “lista_tareas”) y
retorna el string “Tarea agregada correctamente a la lista” ó “La tarea no fue
agregada a la lista” en caso que la tarea se haya agregado o no a la lista
respectivamente.
• Método de instancia público “quitarTarea(tarea)”, que recibe como argumento una
tarea que debe ser eliminada de la lista de tareas (atributo “lista_tareas”) y retorna
el string “Tarea eliminada correctamente de la lista” ó “La tarea no fue eliminada de
la lista” en caso que la tarea se haya eliminado o no de la lista respectivamente.
• Método de instancia público “mostrarTareas()”, que no recibe ningún argumento y
retorna la lista de tareas (atributo “lista_tareas”) .
Luego, se debe crear una instancia de ListaDeTareas, agregar tareas a la lista, eliminar
tareas de la lista y finalmente imprimir la lista completa de tareas.

Nota: el método “quitarTarea(tarea)” debe validar si la tarea a eliminar existe en la lista
antes de ser eliminada.
"""

class ListaDeTareas:
    def __init__(self):
        self.__lista_tareas: list = []

    def agregar_tarea(self, tarea: str):
        tarea_ingresada: str = tarea.strip().lower()
        es_tarea_invalida: bool = len(tarea_ingresada) == 0
        es_tarea_existente: bool = self.__lista_tareas.count(tarea_ingresada) != 0
        if not es_tarea_invalida and not es_tarea_existente:
            self.__lista_tareas.append(tarea_ingresada)
            print(f'"{tarea_ingresada}" fue agregada correctamente a la lista de tareas')
        else:
            print(f'"{tarea_ingresada}" no fue agregada a la lista de tareas')

    def quitar_tarea(self, tarea: str):
        tarea_ingresada: str = tarea.strip().lower()
        es_tarea_existente: bool = self.__lista_tareas.count(tarea_ingresada) != 0
        if es_tarea_existente:
            self.__lista_tareas.remove(tarea_ingresada)
            print(f'"{tarea_ingresada}" fue eliminada correctamente de la lista de tareas')
        else:
            print(f'"{tarea_ingresada}" no fue eliminada de la lista de tareas')

    def mostrar_tareas(self):
        return self.__lista_tareas

if __name__ == "__main__":
    lista_prueba = ListaDeTareas() # instanciamos una lista de tareas vacía
    lista_prueba.agregar_tarea('') # no agrega tareas vacías
    lista_prueba.agregar_tarea('tarea 1') # agrega la tarea correctamente
    lista_prueba.agregar_tarea('      tarea 1     ') # quita los espacios y no agrega tareas repetidas
    lista_prueba.agregar_tarea('TAREA 1') # convierte a minúscula y no agrega tareas repetidas
    lista_prueba.agregar_tarea('tarea 2') # agrega la tarea correctamente
    lista_prueba.agregar_tarea('tarea 3') # agrega la tarea correctamente
    print('Tareas agregadas:', lista_prueba.mostrar_tareas()) # imprime ['tarea 1', 'tarea 2', 'tarea 3']
    lista_prueba.quitar_tarea('tarea 3') # quita la tarea 3
    lista_prueba.quitar_tarea('tarea 3') # no quita una tarea que no existe
    lista_prueba.agregar_tarea('') # no agrega tareas vacías
    lista_prueba.quitar_tarea('') # no quita una tarea que no existe
    print('Tareas agregadas:', lista_prueba.mostrar_tareas()) # imprime ['tarea 1', 'tarea 2']
